package es.cipfpbatoi.psp.u4.producerconsumer;

public class ProducerConsumerTest {

    public static void main(String[] args) {

        //Buffer
        MyBuffer myBuffer = new MyBuffer();

        //Productor y consumidor ( Recurso Compartido myBuffer )
        Producer producer1 = new Producer(myBuffer,1,0,500,false);
        Producer producer2 = new Producer(myBuffer,2,500,1000,false);
        Consumer consumer1 = new Consumer(myBuffer, 1);
        Consumer consumer2 = new Consumer(myBuffer, 2);

        //Creamos los hilos
        Thread producerT1 = new Thread(producer1);
        Thread producerT2 = new Thread(producer2);
        Thread consumerT1 = new Thread(consumer1);
        Thread consumerT2 = new Thread(consumer2);

        //Los arrancamos
        producerT1.start();
        producerT2.start();
        consumerT1.start();
        consumerT2.start();

        //Los esperamos aunque no finalizarán nunca
        try {
            producerT1.join();
            producerT2.join();
            consumerT1.join();
            consumerT2.join();

        } catch (InterruptedException e) {
            System.err.println(e.getLocalizedMessage());
        }


    }

}
