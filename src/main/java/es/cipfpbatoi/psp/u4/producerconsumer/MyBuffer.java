package es.cipfpbatoi.psp.u4.producerconsumer;

public class MyBuffer {

    boolean empty = false;

    public int value;
    public MyBuffer(){
        empty = true;
    }

    //Without arguments, returns the last element.
    synchronized int get(){
        while(isEmpty()){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        empty = true;
        notifyAll();
        return value;
    }

    synchronized void put(int i){
        while (!isEmpty()){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        empty = false;
        value = i;
        notifyAll();
    }

    public boolean isEmpty() {
        return empty;
    }
}
