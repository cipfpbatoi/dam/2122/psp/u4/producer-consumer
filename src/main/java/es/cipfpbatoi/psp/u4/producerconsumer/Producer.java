package es.cipfpbatoi.psp.u4.producerconsumer;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Producer implements Runnable {

    int minIntNumber;
    int maxIntNumber; //Until nine
    boolean repeatRandomNumbers;
    final int MIN_SLEEP_MS = 0;
    final int MAX_SLEEP_MS = 100; //Until 99

    MyBuffer myBuffer;
    int numProducer = -1;

    public Producer(MyBuffer myBuffer, int numProducer, int minIntNumber,int maxIntNumber, boolean repeatRandomNumbers) {
        this.numProducer = numProducer;
        this.myBuffer = myBuffer;
        this.minIntNumber = minIntNumber;
        this.maxIntNumber = maxIntNumber;
        this.repeatRandomNumbers = repeatRandomNumbers;
    }

    public Producer(MyBuffer myBuffer,int numProducer, int minIntNumber,int maxIntNumber)
    {
       this(myBuffer,numProducer,minIntNumber,maxIntNumber,false);
    }

    /**
     * @since 1.0.1
     * @param myBuffer
     * @param numProducer
     */
    public Producer(MyBuffer myBuffer, int numProducer){
        this(myBuffer,numProducer,0,10,true);
    }

    @Override
    public void run() {

        ArrayList<Integer> aleatorios = new ArrayList<>();

        IntStream intGenerator = ThreadLocalRandom.current().ints(minIntNumber,maxIntNumber);

        if (!repeatRandomNumbers) {

            intGenerator = intGenerator.distinct();

        }

        intGenerator.forEach(

                (i)->{

                    int sleep = ThreadLocalRandom.current().nextInt(MIN_SLEEP_MS,MAX_SLEEP_MS);

                    try {
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        System.err.println(e.getLocalizedMessage());
                    }
                    myBuffer.put(i);
                    System.out.printf("Producer %d put: %d%s",numProducer,i,System.lineSeparator());

                }
        );

    }
}
