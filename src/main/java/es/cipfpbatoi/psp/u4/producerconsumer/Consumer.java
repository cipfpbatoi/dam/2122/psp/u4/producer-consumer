package es.cipfpbatoi.psp.u4.producerconsumer;

public class Consumer implements Runnable {

    MyBuffer myBuffer;
    int numConsumer = -1;

    public Consumer(MyBuffer myBuffer, int numConsumer) {
        this.myBuffer = myBuffer;
        this.numConsumer = numConsumer;
    }

    @Override
    public void run() {

        // El consumidor no finaliza hasta que no lo paremos forzosamente
        while (true){

            int i = myBuffer.get();
            System.out.printf("Consumer %d got: %d%s",numConsumer,i,System.lineSeparator());

        }
    }
}
